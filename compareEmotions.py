#!/usr/bin/python

from __future__ import absolute_import, division, print_function
import sys, re

totalEntries = 0.0 #calculated from hive table using count function , 69 entries ignoring dif = 0
successCnt = 0.0
try:
	for line in sys.stdin:
		data = line.split('\t')
		label = str.strip(data[1])      
		dif = int(str.strip(data[4])) #difference in pos - neg

		if(label == "positive" and dif > -2):
			totalEntries = totalEntries + 1
		elif(label == "negative" and dif < -1):
			successCnt = successCnt + 1
		#elif(label == "neutral" and dif == 0):
		#	successCnt = successCnt + 1
		# Output format:
		#hw_num int
		#label String
		#posCount int
		#negCount int
		#dif int

	successRate = successCnt/totalEntries
	print(str(successCnt) + '\t' + str(totalEntries) + '\t' + str(successRate))
except:
	print(sys.exc_info())
