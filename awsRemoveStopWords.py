import sys, re

try:
        for line in sys.stdin:
                data = line.split('\t')
                review = str.strip(data[0])
                formattedReview = str.lower(review)

                formattedReview = re.sub(r"[^\w| |'+]", ' ', formattedReview)
                formattedReview = ' ' + formattedReview + ' '

                with open("english.stop", 'r') as f:
                        for stopWord in f:
                                stopWord = str.strip(stopWord)
                                formattedReview = str.replace(formattedReview, ' ' + stopWord + ' ', ' ')
                print(formattedReview + '\t' + data[1])
execpt:
print sys.exc_info()