CREATE EXTERNAL TABLE json_table ( json string );

LOAD DATA INPATH 's3://427bucket/input/reviews_Musical_Instruments_5.json' INTO TABLE json_table;

CREATE TABLE reviews
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
as select v.reviewText, v.overall
from json_table jt
     LATERAL VIEW json_tuple(jt.json, 'reviewText', 'overall') v
     as reviewText, overall;

ADD FILE s3://427bucket/files/awsRemoveStopWords.py;
ADD FILE s3://427bucket/files/english.stop;

CREATE TABLE filtered AS SELECT TRANSFORM(*) USING 'removeStopWords.py' AS( reviewText string, overall double) FROM reviews;

ADD FILE s3://427bucket/files/avgEmotions.py;
ADD FILE s3://427bucket/files/pos-words.txt;
ADD FILE s3://427bucket/files/neg-words.txt;