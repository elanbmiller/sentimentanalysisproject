--used to preprocess reviews AFTER they've been loaded into a table named 'reviews'
-- in the format (hw_num int, label String, review String)

CREATE TABLE reviews (
hw_num int,
label String,
review String);

--load data

ADD FILE /home/training/cse427s_fl16/final_project/removeStopWords.py;
ADD FILE /home/training/cse427s_fl16/final_project/text/english.stop;

CREATE TABLE filtered AS 
SELECT TRANSFORM(*) USING 'removeStopWords.py' AS (
hw_num int, label String, review String)
FROM reviews;

ADD FILE /home/training/sentimentanalysisproject/avgEmotions.py;
ADD FILE /home/training/cse427s_fl16/final_project/text/pos-words.txt;
ADD FILE /home/training/cse427s_fl16/final_project/text/neg-words.txt;

CREATE TABLE scored AS 
SELECT TRANSFORM(*) USING 'avgEmotions.py' AS (
hw_num int, label String, posCount int, negCount int, score int) FROM filtered;

--view avg score
SELECT hw_num, AVG(score) FROM scored GROUP BY hw_num;

--ngrams stuff

SELECT EXPLODE(CONTEXT_NGRAMS(SENTENCES(review),ARRAY("this", "homework", NULL, NULL),4,3)) as ngrams FROM filteredReviews;


SELECT ngrams(sentences(lower(review)),2, 10 ) FROM filteredReviews;


--Get 250 most popular words and put in table
CREATE TABLE popularWords AS SELECT explode(context_ngrams(sentences(lower(review)), array(null),250)) AS ngrams FROM filteredReviews;


--make data formatted for use

create table popularWords(
top array<struct<ngram:array<string>,estfrequency:double>>);

--**change to array(null,null) for bi grams**
insert overwrite table popularWords select context_ngrams(sentences(lower(review)),array(null),250) as topWords from filteredReviews;


create table formatPopWords(
ngram  array<string>,
estfrequency double);


INSERT OVERWRITE TABLE formatPopWords SELECT topGram.ngram, topGram.estfrequency from popularWords LATERAL VIEW explode(top)words as topGram;

--change column ngram type to string
alter TABLE formatPopWords CHANGE ngram ngram STRING;

--Join with pos words and then neg words
SELECT fpw.ngram, fpw.estfrequency from formatPopWords fpw JOIN posWords pw ON(fpw.ngram = pw.positive) order by fpw.estfrequency DESC;

--RESULT: 
--good 188.0	
--work	153.0
--pretty	107.0
--interesting	93.0
--easy	81.0

SELECT fpw.ngram, fpw.estfrequency from formatPopWords fpw JOIN negWords nw ON(fpw.ngram = nw.negative) order by fpw.estfrequency DESC;

--RESULT:
--problem	337.0
--difficult	131.0
--hard	119.0
--problems	104.0
--pig	99.0


--To get top positive 2 word ngrams, make new table of top 2 word ngrams

create table popularWords2(
top array<struct<ngram:array<string>,estfrequency:double>>);

insert overwrite table negativenewpopularWords2 select context_ngrams(sentences(lower(review)),array(null,null),250) as topWords from filteredReviews;

--**not needed!-- ALTER table popularWords2 CHANGE top top array<string>

create table formatPopWords2(
ngrams  array<string>,
estfrequency double);

INSERT OVERWRITE TABLE formatPopWords2 SELECT topGram.ngram, topGram.estfrequency from popularWords2 LATERAL VIEW explode(top)words as topGram;

--TO get top positive 3 word ngrams, make new table of top 3 word ngrams

create table popularWords3(
top array<struct<ngram:array<string>,estfrequency:double>>);

insert overwrite table popularWords3 select context_ngrams(sentences(lower(review)),array(null,null,null),250) as topWords from filteredReviews;

--**not needed!-- ALTER table popularWords3 CHANGE ngrams ngrams array<string>;

create table formatPopWords3(
ngram  array<string>,
estfrequency double);

INSERT OVERWRITE TABLE formatPopWords3 SELECT topGram.ngram, topGram.estfrequency from popularWords3 LATERAL VIEW explode(top)words as topGram;

--1. query to get top 2-word ngrams where either the first or second word or both are positive words:

SELECT fpw.ngrams, fpw.estfrequency FROM formatPopWords2 fpw JOIN posWords pw ON (fpw.ngrams[0] = pw.positive) ORDER BY fpw.estfrequency DESC
UNION ALL
SELECT fpw.ngrams, fpw.estfrequency FROM formatPopWords2 fpw JOIN posWords pw ON (fpw.ngrams[1] = pw.positive) ORDER BY fpw.estfrequency DESC;

--TOP RESULT:
--["enjoyed","homework"]	22.0
--["homework","good"]	22.0
--["home","work"]	20.0
--["homework","helpful"]	17.0
--["homework","pretty"]	16.0




--2. query to get top 3-word ngrams where word 1 or 2 or 3 or any combination are positive:

SELECT fpw.ngram, fpw.estfrequency FROM negativenewformatPopWords3 fpw JOIN negWords pw ON (fpw.ngram[1] = pw.negative) ORDER BY fpw.estfrequency DESC
UNION ALL
SELECT fpw.ngram, fpw.estfrequency FROM negativenewformatPopWords3 fpw JOIN negWords pw ON (fpw.ngram[0] = pw.negative) ORDER BY fpw.estfrequency DESC
UNION ALL
SELECT fpw.ngram, fpw.estfrequency FROM negativenewformatPopWords3 fpw JOIN negWords pw ON (fpw.ngram[2] = pw.negative) ORDER BY fpw.estfrequency DESC;


--TOP RESULT:
--["back","fun","long"]	5.0
--["1","good","time"]	5.0
--["thought","homework","good"]	4.0
--["homework","assignment","good"]	3.0
--["problem","3","good"]	3.0
--["work","good","comrehensive"]	3.0
--["work","good","comrehensive"]	3.0
--["good","practical","problem"]	3.0
--["happy","part","difficult"]	3.0
--["assignment","run","important"]	3.0


--STEP 4:
--Make table for spring reviews
    > CREATE TABLE spreviews(
    > hw_num int,
    > label String,
    > review String);
--Load negative,positive and neutral files into table
--create filtered table

CREATE TABLE filteredspreviews AS SELECT TRANSFORM(*) USING 'removeStopWords.py' AS (hw_num int, label STRING, review STRING) FROM spreviews;

--get it ready for analysis

CREATE TABLE spAnalysis AS SELECT TRANSFORM(*) USING 'avgEmotions.py' AS(hw_num int, label String, positives int, negatives int, dif int) FROM filteredspreviews;
--Script to determine success
--get number of table entries so you know what to divide by in the script when calculating the avg

select count(*) from spAnalysis;
--90--

--Add compare emotions script
ADD FILE /home/training/cse427s/sentimentanalysisproject/compareEmotions.py;

SELECT TRANSFORM(*) USING 'compareEmotions.py' FROM spAnalysis;


--Improving algorithm
--1. make new table of top 4000 two word n grams(first make new tables containting top 4000 ngrams formatted correctly
CREATE TABLE top2Ngrams AS SELECT fpw.ngrams, fpw.estfrequency FROM newformatPopWords2 fpw JOIN posWords pw ON (fpw.ngrams[1] = pw.positive) ORDER BY fpw.estfrequency DESC
UNION ALL
SELECT fpw.ngrams, fpw.estfrequency FROM newformatPopWords2 fpw JOIN posWords pw ON (fpw.ngrams[0] = pw.positive) ORDER BY fpw.estfrequency DESC;

select * from top2ngrams order by estfrequency desc;

--2. repeat step 1 for negatives (negativetop2ngrams)
--3. make table of top positve and negative ngrams
--4. repeat all this for 3 word ngrams(top3ngrams and negativetop3ngrams)

--move 2/3 word pos/neg tables into textfile

INSERT OVERWRITE LOCAL DIRECTORY '/home/training/cse427s/sentimentanalysisproject/ngramTables/' SELECT * FROM negativetop2ngrams;

-... same thing for the others

--Now we are testing using 2 and 3 word ngrams respectively instead of words
--1. import all 4 files and the script file avgEmotionsNGram.py
--2.run script on each

--create table analysis2(for 2 word ngram) and analysis3(for 3 word)
CREATE TABLE analysis2 AS SELECT TRANSFORM(*) USING 'avgEmotionsNGram.py' AS (pos double, neg double, dif double) FROM filteredspReviews;

CREATE TABLE analysis3 AS SELECT TRANSFORM(*) USING 'avgEmotionsNGram.py' AS (hw_num, label, post, neg, dif) FROM filteredspReviews;


--Run compareEmotions.py on both tables to check success rate

--Run same script on preprocess tables of our own reviews 
