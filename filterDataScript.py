#!/usr/bin/python

import os


for root, dirs, files in os.walk("."):
    path = root.split('/')
    print((len(path) - 1) * '---', os.path.basename(root))
    
    baseDir = os.path.basename(root)
    if(	(baseDir == "hw_reviews_SP2016") | (baseDir == "hw_reviews_FL2016")	):
	updateDir = baseDir   

    for file in files:
	numWords = 0	
	if ((baseDir == 'positive') | (baseDir == 'not_labled') | (baseDir == 'negative') | (baseDir == 'neutral') ):
		newDir = updateDir + "/" + baseDir + "/"		
		fullFilePath = newDir + file		
		print(len(path) * '---', fullFilePath)
		
		#deletes files < 50 words
		with open(fullFilePath, 'r') as f:
			for line in f:				
				words = line.split()
				numWords = numWords + len(words)
			print(numWords)
			if(numWords < 50):
				os.remove(fullFilePath)
				print("file deleted")
				continue
		
		
		#get hw number from file		
		hwNum = file[2] + ""
		if(hwNum == "M"):
			os.remove(fullFilePath)
			continue
		if file[3] != '_':
			hwNum += file[3]
		
		hwNum = int(hwNum)		
	
		#cleans up files (removes spaces and tabs)
		openFile = open(fullFilePath, "r+")
		clean = openFile.read().replace('\n','').replace('\t', '').replace('\r', '')
		openFile.close()
		openFile = open(fullFilePath, "w")
		#separates hw number, label (which is baseDir) and the cleaned up data in the file
		openFile.write(str(hwNum) + '\t' + baseDir + '\t' + clean + '\n')
		




















