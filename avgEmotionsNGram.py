#!/usr/bin/python

import sys, re
try:
		for line in sys.stdin:
				data = line.split('\t')
				hwNum = str.strip(data[0])	
				review = str.strip(data[2])
				
				posCnt = 0
				negCnt = 0
				#must change to "pos3" for trigrams
				with open("pos2", 'r') as f:
						for f_line in f:
							posWord = f_line.split('\t')[0]
							posWord = str.strip(posWord)	
							posCnt = posCnt + str.count(review, posWord) 
				#must change to "neg3" for trigrams
				with open("neg2",'r') as f2:
						for f2_line in f2:
							negWord = f2_line.split('\t')[0]
							negWord = str.strip(negWord)
							negCnt = negCnt + str.count(review, negWord) 

				# Output format:
				#hw_num int
				#label String
				#posCount int
				#negCount int
				#score int
				print(data[0] + '\t' + data[1] + '\t' + 
					str(posCnt) + '\t' + str(negCnt) + '\t' + str(posCnt-negCnt))
except:
		print (sys.exc_info())
