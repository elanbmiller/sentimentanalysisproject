#!/usr/bin/python

import sys, re

try:
	for line in sys.stdin:
		data = line.split('\t')
		review = str.strip(data[2])	
		formattedReview = str.lower(review)

		formattedReview = re.sub(r"[^\w| |'+]", ' ', formattedReview)
		formattedReview = ' ' + formattedReview + ' '

		with open("english.stop", 'r') as f:
			for stopWord in f:
				stopWord = str.strip(stopWord)
				formattedReview = str.replace(formattedReview, ' ' + stopWord + ' ', ' ')
 				
		print(data[0] + '\t' + data[1] + '\t' + formattedReview)

except:
   #In case of an exception, write the stack trace to stdout so that we
   #can see it in Hive, in the results of the UDF call.
   print sys.exc_info() 
