#!/usr/bin/python

import sys, re
try:
		for line in sys.stdin:
				data = line.split('\t')
				hwNum = str.strip(data[0])	
				review = str.strip(data[2])
				
				posCnt = 0
				negCnt = 0
				with open("pos-words.txt", 'r') as f:
						posCnt = 0
						for posWord in f:
							posWord = str.strip(posWord)	
							posCnt = posCnt + str.count(review, posWord) 
				with open("neg-words.txt",'r') as f2:
						for negWord in f2:
							negWord = str.strip(negWord)
							negCnt = negCnt + str.count(review, negWord) 

				# Output format:
				#hw_num int
				#label String
				#posCount int
				#negCount int
				#score int
				print(data[0] + '\t' + data[1] + '\t' + 
					str(posCnt) + '\t' + str(negCnt) + '\t' + str(posCnt-negCnt))
except:
		print sys.exc_info()
